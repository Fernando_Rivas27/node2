const express =require ('express');
const morgan =require('morgan')
const app=express();

//settings

app.set('port',process.env.PORT || 3000);
app.set('json spaces',2);

//middlewares
app.use(morgan('dev'));
app.use(express.urlencoded({extended:false}));
app.use(express.json());
//routes
app.use(require('./routes/index'));
app.use(require('./routes/materias'));
app.use(require('./routes/users'));
app.listen(app.get('port'),()=> {

    console.log('Server Corriendo en el puerto: ',app.get('port'));
});

