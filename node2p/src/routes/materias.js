const {Router}=require('express');

const router=Router();
const materias = require('../sample.json')
router.get('/materias',(req,res)=>{
    res.json(materias)
});

module.exports=router;