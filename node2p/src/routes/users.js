const {Router}=require('express');
const fetch = require('node-fetch');
const router=Router();


router.get('/users',async (req,res)=>{
   const response=await  fetch('https://jsonplaceholder.typicode.com/users');
   const user = await response.json();
   
    res.json(user)
});

module.exports=router;